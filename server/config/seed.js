/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';

var Thing = require('../api/thing/thing.model');
var User = require('../api/user/user.model');

/*Thing.find({}).remove(function() {
  Thing.create({
    name : 'Development Tools',
    image : '/img/demo/00.jpg',
    info : 'Integration with popular tools such as Bower, Grunt, Karma, Mocha, JSHint, Node Inspector, Livereload, Protractor, Jade, Stylus, Sass, CoffeeScript, and Less.',
      age : '25'
  }, {
    name : 'Server and Client integration',
      image : '/img/demo/01.jpg',
    info : 'Built with a powerful and fun stack: MongoDB, Express, AngularJS, and Node.',
      age : '25'
  }, {
    name : 'Smart Build System',
      image : '/img/demo/02.jpg',
    info : 'Build system ignores `spec` files, allowing you to keep tests alongside code. Automatic injection of scripts and styles into your index.html',
      age : '25'
  },  {
    name : 'Modular Structure',
      image : '/img/demo/03.jpg',
    info : 'Best practice client and server structures allow for more code reusability and maximum scalability',
      age : '25'
  },  {
    name : 'Optimized Build',
      image : '/img/demo/04.jpg',
    info : 'Build process packs up your templates as a single JavaScript payload, minifies your scripts/css/images, and rewrites asset names for caching.',
      age : '25'
  },{
    name : 'Deployment Ready',
      image : '/img/demo/05.jpg',
    info : 'Easily deploy your app to Heroku or Openshift with the heroku and openshift subgenerators',
      age : '25'
  });
});*/

User.find({}).remove(function() {
  User.create({
    provider: 'local',
    name: 'Test User',
    email: 'test@test.com',
    password: 'test'
  }, {
    provider: 'local',
    role: 'admin',
    name: 'Admin',
    email: 'admin@admin.com',
    password: 'admin'
  }, function() {
      console.log('finished populating users');
    }
  );
});